#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Various fnmatch- and glob-style handling.
//!
//! For the present, this crate only defines a conversion function from
//! an fnmatch-style glob pattern to a regular expression.
//!
//! See the [`glob`] module for more information on
//! the [`glob_to_regex`] function's usage.

#![allow(clippy::pub_use)]

pub mod error;
pub mod glob;

pub use glob::glob_to_regex;

#[cfg(test)]
pub mod tests;
