// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Common error definitions for the fnmatch crate.

use anyhow::Error as AnyError;
use regex::Error as RexError;
use thiserror::Error;

/// An error that occurred during the processing of a pattern.
#[derive(Debug, Error)]
#[non_exhaustive]
#[allow(clippy::error_impl_error)]
pub enum Error {
    /// A bare escape character at the end of the pattern.
    #[error("Bare escape character")]
    BareEscape,

    /// Something went really, really wrong.
    #[error("fnmatch-regex internal error")]
    InternalError(#[source] AnyError),

    /// The resulting regex was invalid.
    #[error("Could not compile the resulting pattern {0:?}")]
    InvalidRegex(String, #[source] RexError),

    /// Some known missing functionality.
    #[error("Not implemented yet: {0}")]
    NotImplemented(String),

    /// An invalid combination of ranges ([a-b-c]) within a character class.
    #[error("Range following a {0:?}-{1:?} range")]
    RangeAfterRange(char, char),

    /// A reversed range within a character class.
    #[error("Reversed range from {0:?} to {1:?}")]
    ReversedRange(char, char),

    /// An alternation that was not closed before the end of the pattern.
    #[error("Unclosed alternation")]
    UnclosedAlternation,

    /// A character class that was not closed before the end of the pattern.
    #[error("Unclosed character class")]
    UnclosedClass,
}
