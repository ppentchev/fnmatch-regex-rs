// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Unit tests for the `fnmatch-regex` library.

pub mod glob;
