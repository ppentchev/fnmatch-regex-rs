<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the fnmatch-regex project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2024-10-12

### MSRV declaration

- Declare MSRV 1.58 and use inline format string arguments.

### Fixes

- Allow literal matching of characters that do not have any
  special meaning in the glob context, but may have special
  meaning as regular expression metacharacters, such as `(` or `)`.
  Thanks, [Christoph Heer][christophheer]!
- Convert an unreachable panic message to an internal error;
  now the code should really never panic.
- Add a docstring to the unit tests top-level module.

### Additions

- Add the `InternalError` value to the `Error` enum.
- Start some `MkDocs` documentation.

### Other changes

- Move the Clippy configuration to a `run-clippy.sh` tool.
- Keep the `Cargo.lock` file under version control.
- Switch from `quickerror` to `thiserror` for the `Error` enum.
- Use `itertools` 0.13 and `rstest` 0.23 with no changes.
- Insist that all public and private items be documented.
- Specify minimum versions for the dependencies as found in
  the Debian stable distribution.
- Mark the `Itertools` trait as unused by name.
- Drop `mkdocstrings` and `mkdocstrings-python` from the documentation
  `pip` requirements file, we do not use them.

## [0.2.0] - 2022-06-11

### INCOMPATIBLE CHANGES

- The `fnmatch_regex::error::Error` class is now
  an enum that uses the quick-error library, not our own hand-rolled
  struct any more.
- The `glob_to_regex()` function returns a plain
  error object now, not a boxed one.

### Fixes

- Refactor the code to follow Rust best practices and some Clippy
  suggestions; among other things, the code will no longer panic.

### Additions

- Add an EditorConfig definitions file.
- Add the `categories` and `keywords` Cargo package attributes.

### Other changes

- Switch to Rust 2021 edition.
- Refactor the code to avoid pushing to strings and vectors, using
  some internal iterator/adapter structs instead.
  Thanks to Kevin Reid for a couple of iterator-related suggestions!
- Use the rstest library for data-driven testing instead of doing it
  by ourselves.
- Use the itertools library to simplify some operations a whole lot.
  Thanks again to Kevin Reid for pointing it out to me!

## [0.1.0] - 2021-06-22

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/fnmatch-regex-rs/-/compare/release%2F0.2.1...main
[0.2.1]: https://gitlab.com/ppentchev/fnmatch-regex-rs/-/compare/release%2F0.2.0...release%2F0.2.1
[0.2.0]: https://gitlab.com/ppentchev/fnmatch-regex-rs/-/compare/release%2F0.1.0...release%2F0.2.0
[0.1.0]: https://gitlab.com/ppentchev/fnmatch-regex-rs/-/tags/release%2F0.1.0

[christophheer]: https://gitlab.com/christophheer "Christoph Heer"
